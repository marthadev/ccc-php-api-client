# CCC Client

[![Latest Version on Packagist](https://img.shields.io/packagist/v/layer7/ccc-api.svg?style=flat-square)](https://packagist.org/packages/layer7/ccc-api)
[![Total Downloads](https://img.shields.io/packagist/dt/layer7/ccc-api.svg?style=flat-square)](https://packagist.org/packages/layer7/ccc-api)


This is where your description should go. Try and limit it to a paragraph or two. Consider adding a small example.

## Installation

You can install the package via composer:

```bash
composer require layer7/ccc-api
```

## Usage

[CCC API Docs](https://api-doc.ccc.uno/)

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Jacobo Salazar](https://github.com/jackpump)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
